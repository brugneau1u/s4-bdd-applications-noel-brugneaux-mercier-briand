<?php
namespace BDD\controleur;
//require_once realpath(__DIR__ . '/../vendor/autoload.php');
//require __DIR__.'../vendor/autoload.php';
use \BDD\models\Annonce;
use \BDD\models\Photo;
use \BDD\models\Categorie;

class Controleur {

  public function requete1(){
    echo "Faker s'installe grâce à composer.
    On créé une instance de Faker('Faker\Factory::create()').
    On peut ensuite utiliser des attributs de Faker comme 'name' qui
    permettent de créer des nom aléatoires.";
  }

  public function requete2($faker){
    echo $faker->address;
  }

  public function requete3(){
    $date = date("Y/m/d");
    $heure = date("H:i");
    $dateTime = new \DateTime($date);
    $heureTime = new \DateTime($heure);
    $t = $dateTime->format("Y/m/d");
    $h = $heureTime->format("H:i");
    echo "$t ($h)";
  }
}
