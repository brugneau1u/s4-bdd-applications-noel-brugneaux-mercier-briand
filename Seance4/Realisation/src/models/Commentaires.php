<?php

namespace BDD\models;

class Commentaires extends \Illuminate\Database\Eloquent\Model{

	protected $table='commentaires';
	protected $primaryKey='id';
	public $timestamps=true;

	public function user(){
		return $this->belongsTo('BDD\models\Users','email');
	}

	public function game(){
		return $this->belongsTo('BDD\models\Game','id_game');
	}

}
