<?php

namespace BDD\controleur;

use \BDD\models\Game;
use \BDD\models\Company;
use \BDD\models\Platform;
use \BDD\models\Character;
use \BDD\models\Rating;
use \BDD\models\Rating_board;
use \BDD\models\Users;
use \BDD\models\Commentaires;


class Controleur{

	public function requete1(){
	$users1 = new Users();
	$users1->email = 'eeeee@ffff.fr';
	$users1->nom = 'aa';
	$users1->prenom = 'bb';
	$users1->adresse = 'efersfze';
	$users1->numTel = '468456';
	$users1->dateNaiss = '2000-10-20';
	$users1->save();

	$users2 = new Users();
	$users2->email = 'rgfs@tgg.fr';
	$users2->nom = 'acc';
	$users2->prenom = 'dd';
	$users2->adresse = 'eyjhygjyg';
	$users2->numTel = '545564';
	$users2->dateNaiss = '1992-12-01';
	$users2->save();
	echo 'Users ajouté';

	$commentaires1 = new Commentaires();
	$commentaires1->titre = 'titre';
	$commentaires1->contenu = 'contenu';
	$commentaires1->id_game= 12342;
	$commentaires1->email = 'eeeee@ffff.fr';
	$commentaires1->save();

	$commentaires2 = new Commentaires();
	$commentaires2->titre = 'titre';
	$commentaires2->contenu = 'contenu';
	$commentaires2->id_game= 12342;
	$commentaires2->email = 'eeeee@ffff.fr';
	$commentaires2->save();

	$commentaires3 = new Commentaires();
	$commentaires3->titre = 'titre';
	$commentaires3->contenu = 'contenu';
	$commentaires3->id_game= 12342;
	$commentaires3->email = 'eeeee@ffff.fr';
	$commentaires3->save();

	$commentaires4 = new Commentaires();
	$commentaires4->titre = 'titre';
	$commentaires4->contenu = 'contenu';
	$commentaires4->id_game= 12342;
	$commentaires4->email = 'rgfs@tgg.fr';
	$commentaires4->save();

	$commentaires5 = new Commentaires();
	$commentaires5->titre = 'titre';
	$commentaires5->contenu = 'contenu';
	$commentaires5->id_game= 12342;
	$commentaires5->email = 'rgfs@tgg.fr';
	$commentaires5->save();

	$commentaires6 = new Commentaires();
	$commentaires6->titre = 'titre';
	$commentaires6->contenu = 'contenu';
	$commentaires6->id_game= 12342;
	$commentaires6->email = 'rgfs@tgg.fr';
	$commentaires6->save();

  }


	public function requete2($faker){
		$user;
		for ($i=1; $i <= 25000; $i++) {
			$user = new Users();
			$user->email = $faker->unique->email;
			$user->nom = $faker->firstNameMale;
			$user->prenom = $faker->lastName;
			$user->adresse = $faker->address;
			$user->numTel = $faker->phoneNumber;
			$user->dateNaiss = $faker->date($format = 'Y-m-d', $max = 'now');
			$user->save();
		}
		echo "Les users ont bien été créés";
	}

	public function requete3($faker){
		$commentaire;
		$id_game;
		$email_user = Users::select('email')->get();
		for ($i=1; $i <= 250000; $i++) {
		$commentaire = new Commentaires();
		$commentaire->titre = $faker->word;
		$commentaire->contenu = $faker->text($maxNbChars = 300);
		$commentaire->id_game= rand(1, 47948);
		$commentaire->email = $email_user[rand(0, 25001)]->email;
		$commentaire->save();
		}
		echo "Les commentaires ont bien été créés";
	}
	
	  public function requete4(){
	  $user = Users::where('email','=','eeeee@ffff.fr')->first();
	  $commentaires = $user->commentaires;
	  foreach($commentaires as $commentaire){
		  echo $commentaire->titre . '<br>' . $commentaire->contenu . ' fais le ' . $commentaire->created_at . '<br><br>';
	  }
  }
  
  public function requete5(){
	$users = Users::get();
	foreach($users as $user){
		$size = $user->commentaires()->get();
		if(sizeof($size) > 5)
		  echo $user->email .'<br>';
	}
  }
}
