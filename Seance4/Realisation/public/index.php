<?php
set_time_limit (0);
require_once '../vendor/autoload.php' ;
use \BDD\controleur\Controleur;
use \Illuminate\Database\Capsule\Manager as DB;

$db=new DB();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();


$app->get('/partie1/requete1',function(){
  $controleur=new Controleur();
  $controleur->requete1();
});

$app->get('/partie2/requete1',function(){
  $controleur=new Controleur();
  $faker = Faker\Factory::create('en_US');
  $controleur->requete2($faker);
});

$app->get('/partie2/requete2',function(){
  $controleur=new Controleur();
  $faker = Faker\Factory::create('en_US');
  $controleur->requete3($faker);

});

$app->get('/partie2/requete3',function(){
  $controleur=new Controleur();
  $controleur->requete4();
});

$app->get('/partie2/requete4',function(){
  $controleur=new Controleur();
  $controleur->requete5();
});	

$app->run();