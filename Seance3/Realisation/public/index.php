<?php
require_once '../vendor/autoload.php' ;
use \BDD\controleur\Controleur;
use \Illuminate\Database\Capsule\Manager as DB;

$db=new DB();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();


$app->get('/partie4/requete1',function(){
  $controleur=new Controleur();
  $controleur->requete1();
});

$app->get('/partie4/requete2',function(){
  $controleur=new Controleur();
  $controleur->requete2();
});

$app->get('/partie4/requete3',function(){
  $controleur=new Controleur();
  $controleur->requete2();
});

$app->get('/partie4/requete4',function(){
  $controleur=new Controleur();
  $controleur->requete2();
});

$app->get('/partie4/requete5',function(){
  $controleur=new Controleur();
  $controleur->requete5();
});

$app->get('/partie4/requete6',function(){
  $controleur=new Controleur();
  $controleur->requete6();
});

$app->get('/partie4/requete7',function(){
  $controleur=new Controleur();
  $controleur->requete7();
});

$app->run();