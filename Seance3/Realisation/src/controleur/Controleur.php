<?php

namespace BDD\controleur;

use \BDD\models\Game;
use \BDD\models\Company;
use \BDD\models\Platform;
use \BDD\models\Character;
use \BDD\models\Rating;
use \BDD\models\Rating_board;

use \Illuminate\Database\Capsule\Manager as DB;

class Controleur{	
	
  public function requete1(){
	$games = Game::paginate(500);
	$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];

	echo "La requete dure $time secondes\n";
  }
  
  public function requete2(){
	$games = Game::where('name','like','%Mario%')->get();
	$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];

	echo "La requete dure $time secondes\n";
  }
  
  public function requete3(){
	$games = Game::where('name','like','Mario%')->get();
	foreach($games as $game){
		$characters = $game->characters;
		foreach($characters as $character){
		  echo $character->name. ' ' . $character->deck . '<br>';
		}
	}
	$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];

	echo "La requete dure $time secondes\n";
  }
  
  public function requete4(){
	$games = Game::where('name','like','Mario%')->get();
	foreach($games as $game){
		$ratings = $game->ratings()->where('name', 'like', '%3+%')->get();
		if ($ratings !== NaN)
		  echo $game->name .'<br>'; 
	}
	$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];

	echo "La requete dure $time secondes\n";
  }
  
   public function requete5(){
	   
	$games = Game::where('name','like','WWE%')->get();
	$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
	
	echo "1) La requete qui debute par Mario dure 0.9219810962677 secondes<br>";
	echo "La requete qui debute par Desert dure 0.89147615432739 secondes<br>";
	echo "La requete qui debute par WWE dure 0.85405707359314 secondes<br><br>";
		
	echo "3) La requete qui debute par Mario dure 0.20803117752075 secondes<br>";
	echo "La requete qui debute par Desert dure 0.17911100387573 secondes<br>";
	echo "La requete qui debute par WWE dure 0.18545007705688 secondes<br>";
  }
  
   public function requete6(){
	   
	$c = 0;   
	DB::enableQueryLog();
	
	$jeux = Game::where('name','like','%Mario%')->get();
	echo '1:<br>';
	foreach($jeux as $jeu){
		echo $jeu->name.'<br>';
	}
	
	$query = DB::getQueryLog();
	echo('<br>'.$query[0]["query"].'<br><br>');
	
	$characters = Game::find(12342)->characters()->get();
	foreach($characters as $character){
		echo $character->name.'<br>';
	}
	
	$query = DB::getQueryLog();
	echo('<br>'.$query[0]["query"].'<br><br>');
	
	$games = Game::where('name','like','Mario%')->get();
	foreach($games as $game){
		$characters = $game->characters;
		foreach($characters as $character){
		  echo $character->name. ' ' . $character->deck . '<br>';
		}
	}
	
	$query = DB::getQueryLog();
	echo('<br>'.$query[0]["query"].'<br><br>');
	
	$companies = Company::where('name','like','%Sony%')->get();
	foreach($companies as $company){
		$games = $company->games;
		foreach($games as $game){
		  echo $game->name. '<br>';
		}
	}
	
	$query = DB::getQueryLog();
	echo('<br>'.$query[0]["query"].'<br><br>');
	
	echo("Le nombre de requetes est : " . sizeof($query));
	
	
  }
  
  public function requete7(){
	  $games = Game::where('name','like','Mario%')->get();
	foreach($games as $game){
		$characters = $game->with('characters')->get() ;;
		foreach($characters as $character){
		  echo $character->name. ' ' . $character->deck . '<br>';
		}
	}
	
	$query = DB::getQueryLog();
	echo('<br>'.$query[0]["query"].'<br><br>');
  }
  

  
}
