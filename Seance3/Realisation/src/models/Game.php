<?php

namespace BDD\models;

class Game extends \Illuminate\Database\Eloquent\Model{
	
	
	protected $table='game';
	protected $primaryKey='id';
	public $timestamps=false;
	
	public function characters(){
		return $this->belongsToMany('BDD\models\Character','game2character','game_id','character_id');
	}
	
	public function companies(){
		return $this->belongsToMany('BDD\models\Company','game2character','game_id','comp_id');
	}
	
	public function ratings(){
		return $this->belongsToMany('BDD\models\Rating','game2rating','game_id','rating_id');
	}
}