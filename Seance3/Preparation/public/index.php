<?php
require_once '../vendor/autoload.php' ;
use \BDD\controleur\Controleur;
use \Illuminate\Database\Capsule\Manager as DB;

$db=new DB();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();


$app->get('/partie1/requete1',function(){
  $controleur=new Controleur();
  $controleur->requete1();
});

$app->get('/partie1/requete2',function(){
  $controleur=new Controleur();
  $controleur->requete2();
});

$app->get('/partie2/requete1',function(){
  $controleur=new Controleur();
  $controleur->requete3(); // Permet de recup les logs
});

$app->get('/partie2/requete2',function(){
  $controleur=new Controleur();
  $controleur->requete4();
});

$app->run();
