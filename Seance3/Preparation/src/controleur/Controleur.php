<?php

namespace BDD\controleur;

use \BDD\models\Annonce;
use \BDD\models\Photo;
use \BDD\models\Categorie;

class Controleur{

  public function requete1(){
    // Durée d'attente aléatoire
    usleep(1000000);

    // Temps actuel - le temps d'exec de la requete du serveur
    $duree = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];

    echo "Le script a durée $duree secondes.";
  }

  public function requete2(){
    echo "Les index permettent de parcourrir des lignes de la base de données plus efficace et rapide.\nLe principe est d'utiliser des indexage pour trier les données dans la table ou encore de trier celle-ci par ordre alphabétique pour gagner du temps, comme un tri par nom par exemple.\nIl est également possible de trier sur plusieurs colonne pour accelerer le traitement du serveur MySql.";
  }

  public function requete3(){
    echo "Les logs de requêtes est un journal contenant
    toutes les requêtes qui ont été executé sur le
    serveur. Il est possible de desactiver ce journal.";
  }

  public function requete4(){
    echo "Le problème des N+1 query est le temps
    d'execution des requêtes sur le serveur.
    Sur le site de Laravel, le fait de rechercher les
    auteurs de chaques livres présent dans une base
    demandera l'execution d'une première requête pour
    récuperer tous les livres de la base en appelant
    la fonction 'with', on réduit concidérablement
    le nombre de requête envoyé sur le serveur pour
    gagner en temps d'execution.";
  }
}
