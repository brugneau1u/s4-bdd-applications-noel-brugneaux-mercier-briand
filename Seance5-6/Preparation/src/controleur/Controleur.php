<?php
namespace BDD\controleur;

use \BDD\models\Annonce;
use \BDD\models\Photo;
use \BDD\models\Categorie;

class Controleur {

  public function requete1(){
    echo "Pour retourner un tableau sous la forme json, on fait json_encode($array). Et pour retourner un objet json, on fait json_encode($array, JSON_FORCE_OBJECT) ";
  }

  public function requete2(){
    $app = \Slim\Slim::getInstance();
    echo "Pour accéder aux données dans une URL avec Slim<br>
    il faut écrire '\$app->get('/game/:id', function(\$id)'<br>
    par exemple avec :id qui va être récupéré par slim<br>
    ou '\$app->request->get()'
    -----------------------------------------------------<br>
    Pour les requete post on peut utiliser<br>
    '\$app->request->post()' dans le corps de la route slim";
  }

  public function requete3(){
    echo "\$app = \Slim\Slim::getInstance();<br>
    \$app->halt( 403, 'Forbidden : acces control failed');<br>
    pour un code de retour 403<br>
    -----------------------------------------<br>
    Pour positioner un header on fait<br>
    '\$app->response->headers->set('Content-Type', '…')";
  }
}
