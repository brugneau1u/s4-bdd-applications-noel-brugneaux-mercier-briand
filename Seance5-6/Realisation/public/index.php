<?php
require_once '../vendor/autoload.php' ;
use \BDD\controleur\Controleur;
use \Illuminate\Database\Capsule\Manager as DB;

$db=new DB();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();

/*
$app->get('/api/games/:id',function($id){
  $controleur=new Controleur();
  $controleur->requete1($id);
});
*/

/*
$app->get('/api/games',function(){
  $controleur=new Controleur();
  $controleur->requete2();
});
*/

// partie 3 pagination
/*
$app->get('/api/games',function(){
  $controleur=new Controleur();
  $controleur->requete3();
});
*/

$app->get('/api/games',function(){
  $controleur=new Controleur();
  $controleur->requete4();
});

$app->get('/api/games/:id/comments',function($id){
  $controleur=new Controleur();
  $controleur->requete5($id);
});

$app->get('/api/games/:id',function($id){
  $controleur=new Controleur();
  $controleur->requete6($id);
});

$app->get('/api/games/:id/characters',function($id){
  $controleur=new Controleur();
  $controleur->requete7($id);
});

$app->post('/api/games/:id/comments',function($id){
  $controleur=new Controleur();
  $controleur->requete8($_POST, $id);
});

$app->run();
