<?php

namespace BDD\models;

class Rating extends \Illuminate\Database\Eloquent\Model{
	
	
	protected $table='game_rating';
	protected $primaryKey='id';
	public $timestamps=false;
	
	public function games(){
		return $this->belongsToMany('BDD\models\Game','game2rating','rating_id','game_id');
	}
	public function rating_board(){
		return $this->belongsTo('BDD\models\Rating_board','rating_board_id');
	}
	
}