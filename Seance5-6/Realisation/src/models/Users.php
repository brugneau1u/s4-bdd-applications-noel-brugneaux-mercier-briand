<?php

namespace BDD\models;

class Users extends \Illuminate\Database\Eloquent\Model{


	protected $table='users';
	protected $primaryKey='email';
	public $timestamps=false;
	public $incrementing = false;

	public function commentaires(){
		return $this->hasMany('BDD\models\Commentaires','email');
	}

}
