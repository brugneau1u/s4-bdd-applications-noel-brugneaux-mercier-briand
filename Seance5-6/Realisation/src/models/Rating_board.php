<?php

namespace BDD\models;

class Rating_board extends \Illuminate\Database\Eloquent\Model{
	
	
	protected $table='rating_board';
	protected $primaryKey='id';
	public $timestamps=false;
	
	public function ratings(){
		return $this->hasMany('BDD\models\Rating','id');
	}
	
}