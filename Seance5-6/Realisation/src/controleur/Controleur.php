<?php

namespace BDD\controleur;

use \BDD\models\Game;
use \BDD\models\Company;
use \BDD\models\Platform;
use \BDD\models\Character;
use \BDD\models\Rating;
use \BDD\models\Rating_board;
use \BDD\models\Commentaires;

class Controleur{

  public function requete1($id){
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
    echo Game::where('id', '=', $id)->get();
  }

  public function requete2(){
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
    $res['games'] = Game::select('id','name', 'alias', 'deck')->take(200)->get();
    echo json_encode($res);
  }

  public function requete3(){
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
    $results = Game::paginate(200);
    $result=json_encode($results);
    echo $result;
  }

  public function requete4(){
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
    $nb = 200;
    $gameDb = Game::select('id','name', 'alias', 'deck')->take($nb)->get();

    $game['games'] = [];
    foreach ($gameDb as $key => $value) {
      $gameLink['game'] = $value;
      $idGame = $value['id'];
      $gameLink['links'] = ["self" => ["href" => "/api/games/$idGame"]];
      array_push($game['games'], $gameLink);
    }
    echo json_encode($game);
  }

  public function requete5($id){
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
    echo Commentaires::select('id', 'titre', 'contenu', 'created_at', 'email')->where('id_game', '=', $id)->get();
  }

  public function requete6($id){
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
    $res = [];
    $res['game'] = Game::where('id', '=', $id)->get()[0];
    $res['links'] = ["comments" => ["href" => "/api/games/$id/comments"], "characters" => ["href" => "/api/games/$id/comments"]];
    $platforme = ["id" => "?", "nom" => "?", "alias" => "?", "abreviation" => "?", "url" => "?"];
    $res['platforms'] = [];

    // boucle for à faire pour recup toutes les platformes
    foreach (Game::find($id)->platforms as $key => $value) {
      $res['platforms'][$value->name] = $value;
    }
    echo json_encode($res);
  }

  public function requete7($id){
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
    $res['characters'] = [];

    // boucle for à faire pour recup tous les characters
    foreach (Game::find($id)->characters as $key => $value) {
      array_push($res['characters'], ["character" => ["id" => $value->id, "name" => $value->name], "links" => ["href" => "/api/characters/$value->id"]]);
    }
    echo json_encode($res);
  }

  public function requete8($post, $id){
    $app = \Slim\Slim::getInstance();
    $com = new Commentaires();
    $com->email = $post['email'];
    $com->titre = $post['titre'];
    $com->contenu = $post['contenu'];
    $com->id_game = $id;
    $com->save();
    $app->response->setStatus(201);
    echo "Commentaire ajouté";
  }
}
