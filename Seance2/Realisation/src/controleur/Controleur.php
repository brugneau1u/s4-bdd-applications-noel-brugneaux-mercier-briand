<?php

namespace BDD\controleur;

use \BDD\models\Game;
use \BDD\models\Company;
use \BDD\models\Platform;
use \BDD\models\Character;
use \BDD\models\Rating;
use \BDD\models\Rating_board;
use \BDD\models\Genre;

class Controleur{
  
  public function requete1(){
	$characters = Game::find(12342)->characters()->get();
	foreach($characters as $character){
		echo $character->name. ' ' . $character->deck . '<br>';
	}
  }

  public function requete2(){
	$games = Game::where('name','like','Mario%')->get();
	foreach($games as $game){
		$characters = $game->characters;
		foreach($characters as $character){
		  echo $character->name. ' ' . $character->deck . '<br>';
		}
	}
  }
	
  public function requete3(){	
	$companies = Company::where('name','like','%Sony%')->get();
	foreach($companies as $company){
		$games = $company->games;
		foreach($games as $game){
		  echo $game->name. '<br>';
		}
	}
  }
		
  public function requete4(){
	$games = Game::where('name','like','%Mario%')->get();
	foreach($games as $game){
		$ratings = $game->ratings;
		foreach($ratings as $rating){
		  echo $rating->name;
		  $ratingb = $rating->rating_board;
		  echo ' : ' . $ratingb->name .'<br>';
		}
	}
  }
	
  public function requete5(){
	$games = Game::where('name','like','Mario%')->get();
	foreach($games as $game){
		$size = $game->characters()->get();
		if(sizeof($size) >= 3)
		  echo $game->name.'<br>';
	}
	
  }
  
public function requete6(){
	$games = Game::where('name','like','Mario%')->get();
	foreach($games as $game){
		$ratings = $game->ratings()->where('name', 'like', '%3+%')->get();
		if ($ratings !== NaN)
		  echo $game->name .'<br>'; 
	}
}

public function requete7(){
	$games = Game::where('name','like','Mario%')->get();
	foreach($games as $game){
		$games = $game->ratings()->where('name','like','%3+%')->get();

		$games = $game->companies()->where('name','like','%Inc%')->get();

		foreach($games as $game){
			echo $game->name .'<br>';
		}
	}
}
	
	public function requete8(){
		$games = Game::where('name','like','Mario%')->get();
		foreach($games as $game){
			$games = $game->ratings()->where('name','like','%3+%')->get();
			echo $games . '<br>';

			$games = $game->companies()->where('name','like','%Inc%')->get();
			echo $games .'<br>';

			$games = $game->rating_board()->where('rating_board_id','=','3')->get();
			echo $games .'<br>';
			
			foreach($games as $game){
				echo $game->name .'<br>';
			}
		}
	}

	public function requete9(){

		$var = Genre::find(51);
		if($var == null){
			$data = array('id'=>'51',
								  'name' => 'requete9',
									'deck' => 'null',
									'description' => 'genre requete 9 séance 2');
			Genre::insert($data);
			echo 'ajout du genre requete9 avec id=51<br>';
		}
		


		$game = Game::find(12);
		$game->genre()->attach('51');
		echo 'ajout liaison entre jeu id=12 et genre id=51<br>';

		$game = Game::find(56);
		$game->genre()->attach('51');
		echo 'ajout liaison entre jeu id=56 et genre id=51<br>';

		$game = Game::find(345);
		$game->genre()->attach('51');
		echo 'ajout liaison entre jeu id=345 et genre id=51<br>';
		
	}

}
  
