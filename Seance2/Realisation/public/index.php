<?php
require_once '../vendor/autoload.php' ;
use \BDD\controleur\Controleur;
use \Illuminate\Database\Capsule\Manager as DB;

$db=new DB();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();


$app->get('/partie3/requete1',function(){
  $controleur=new Controleur();
  $controleur->requete1();
});

$app->get('/partie3/requete2',function(){
  $controleur=new Controleur();
  $controleur->requete2();
});
  
$app->get('/partie3/requete3',function(){
  $controleur=new Controleur();
  $controleur->requete3();
});

$app->get('/partie3/requete4',function(){
  $controleur=new Controleur();
  $controleur->requete4();
});

$app->get('/partie3/requete5',function(){
  $controleur=new Controleur();
  $controleur->requete5();
});

$app->get('/partie3/requete6',function(){
  $controleur=new Controleur();
  $controleur->requete6();
});

$app->get('/partie3/requete7',function(){
  $controleur=new Controleur();
  $controleur->requete7();
});

$app->get('/partie3/requete8',function(){
  $controleur=new Controleur();
  $controleur->requete8();
});

$app->get('/partie3/requete9',function(){
  $controleur=new Controleur();
  $controleur->requete9();
});

$app->run();