<?php

namespace BDD\controleur;

use \BDD\models\Annonce;
use \BDD\models\Photo;
use \BDD\models\Categorie;

class Controleur{
  
  public function requete1(){
	$photos = Annonce::find(22)->photos;
  }

  public function requete2(){
	$photos = Annonce::find(22)->photos()->where('taille_octet', '>', '100000')->get();
  }
	
  public function requete3(){	
	$photos = Annonce::photos()->where('count(id)','>','3')->with('annonce');
  }
		
  public function requete4(){
	$photos = Annonce::photos()->where('taille_octet', '>', '100000')->with('annonce');
  }
	
  public function requete5(){
	$annonce = Annonce::find(22);
	$photo = new Photo();
	$photo->file = '...';
	$photo->date= '...';
	$photo->taille_octet= 1000000;
	$annonce->photos()->save($photo);
  }
  
  public function requete6(){
	$annonce = Annonce::find(22);
	$categorie1 = Categorie::find(42);
	$categorie2 = Categorie::find(73);
	$categorie1->annonces()->save($annonce);
	$categorie2->annonces()->save($annonce);
	}
	
}
