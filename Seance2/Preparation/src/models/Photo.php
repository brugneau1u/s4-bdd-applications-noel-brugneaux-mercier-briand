<?php

namespace BDD\models;

use \BDD\models\Annonce;

class Photo extends \Illuminate\Database\Eloquent\Model{
	
	
	protected $table='photo';
	protected $primaryKey='id';
	public $timestamps=false;
	
	public function annonce(){
		return $this->belongsTo('Annonce','id');
	}
	
	
}