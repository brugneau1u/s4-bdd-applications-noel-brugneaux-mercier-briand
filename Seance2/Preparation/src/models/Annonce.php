<?php

namespace BDD\models;

use \BDD\models\Photo;
use \BDD\models\Categorie;


class Annonce extends \Illuminate\Database\Eloquent\Model{
	
	
	protected $table='annonce';
	protected $primaryKey='id';
	public $timestamps=false;
	
	
	public function photos(){
		return $this->hasMany('Photo','id');
	}
	
	public function categories(){
		$this->belongsToMany('Categorie');
	}
	
}