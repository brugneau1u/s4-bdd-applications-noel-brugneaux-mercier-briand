<?php

namespace BDD\models;

use \BDD\models\Annonce;

class Categorie extends \Illuminate\Database\Eloquent\Model{
	
	
	protected $table='categorie';
	protected $primaryKey='id';
	public $timestamps=false;
	
	
	public function annonces(){
		$this->belongsToMany('Annonce');
	}
	
	
	
}