<?php

namespace BDD\controleur;

use \BDD\models\Game;
use \BDD\models\Company;
use \BDD\models\Platform;

class Controleur{
  
  public function requete1(){
	$jeux = Game::where('name','like','%Mario%')->get();
	echo '1:<br>';
	foreach($jeux as $jeu){
		echo $jeu->name.'<br>';
	}
  }

  public function requete2(){
	$companies = Company::where('location_country','=','Japan')->get();
	echo '<br>2:<br>';
	foreach($companies as $company){
		echo $company->name.'<br>';
	}
  }
	
  public function requete3(){	
	$platforms = Platform::where('install_base','>=',10000000)->get();
	echo'<br>3:<br>';
	foreach($platforms as $platform){
		echo $platform->name.'<br>';
	}
  }
		
  public function requete4(){
	echo'<br>4:<br>';
	$jeux = Game::take(442)->skip(21172)->get();
	foreach($jeux as $jeu){
		echo $jeu->id . ' : ' . $jeu->name .'<br>';
	}
  }
	
  public function requete5(){
	echo'<br>5:<br>';
	$jeux = Game::paginate(500);
	foreach($jeux as $jeu){
		echo $jeu->name . ' : ' . $jeu->deck .'<br>';
	}
  }
}
